const exphbs = require('express-handlebars');

const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');

app.get('/', (req, res) => {
    res.render('home', {
        showTitle: true
    });
});

app.listen(port, () => {
    console.log(`Handlebars example running at http://localhost:${port}`);
});
